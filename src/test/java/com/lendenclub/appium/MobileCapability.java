package com.lendenclub.appium;

import java.io.File;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.lendenclub.utility.ExcelSheet;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import net.dongliu.apk.parser.ApkFile;
import net.dongliu.apk.parser.bean.ApkMeta;


public class MobileCapability {

	public static AndroidDriver<MobileElement> driver;
	public static ExcelSheet excel;
	static String appPath = System.getProperty("user.dir")+"/Resources/app-qa.apk";
	
	
	public static AndroidDriver<MobileElement> MobileCapability()
	{
		    DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
			dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
			dc.setCapability(MobileCapabilityType.APP, appPath);
			dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".MainActivity");
			try 
			{
			URL url = new URL("http:127.0.0.1:4723/wd/hub");
			driver = new AndroidDriver<MobileElement>(url, dc);
			
			ApkFile apkFile = new ApkFile(new File(appPath));
			ApkMeta apkMeta = apkFile.getApkMeta();
			String versionName = apkMeta.getVersionName();
			String Name = apkMeta.getName();
			String versionCode = apkMeta.getVersionCode().toString();
			System.out.println();
			System.out.println("**Device Capability is looking good**");
			System.out.println("Executed by : Shubham Sharma");
			System.out.println("Automation Name :"+driver.getAutomationName());
			System.out.println("Plateform Name : "+driver.getPlatformName());
			System.out.println("Name of Application :"+Name);
			System.out.println("Version of "+Name+" Application : "+versionName);
			System.out.println();
			
			excel=new ExcelSheet("./Resources/Investor Details.xls");
			 } 
			catch (Exception e) 
			 {
//    			System.out.println(e.getCause());
//				System.out.println(e.getMessage());	
			 }
			
			return driver;		
			
	}

	public ExcelSheet getExcel()
	{
		return excel;
	}	
	
}
