package com.lendenclub.testclass;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.lendenclub.appium.MobileCapability;
import com.lendenclub.registration.steps.AddBankAccount;
import com.lendenclub.registration.steps.BasicDetails;
import com.lendenclub.registration.steps.ConfirmAddress;
import com.lendenclub.registration.steps.IdDetails;
import com.lendenclub.registration.steps.LegalAuthorization;
import com.lendenclub.registration.steps.LiveKYC;
import com.lendenclub.registration.steps.Onboarding;
import com.lendenclub.registration.steps.RegistrationFees;
import com.lendenclub.registration.steps.SetPassword;
import com.lendenclub.registration.steps.SignUP;
import com.lendenclub.utility.ExcelSheet;
import com.lendenclub.utility.ScreenshotHandler;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Listeners(ScreenshotHandler.class)
public class Investor_Registration extends MobileCapability
{
	
	public static AndroidDriver<MobileElement> driver=MobileCapability();
	public static ExcelSheet excel;
	
	public ExcelSheet getExcel() {
		
		return super.getExcel();
	}
	
	public static void setDriver(AndroidDriver<MobileElement> driver) 
	{
		Investor_Registration.driver = driver;
	}
	public static AndroidDriver<MobileElement> getDriver()
	{
		return driver;
	}
	
	@Test(priority = 1)
	public void test_001OnboardingPage() throws Exception 
	{
		new Onboarding(driver);
	}
	@Test(priority = 2)
	public void test_002SignUPPage() throws Exception 
	{
		new SignUP(driver,excel);
	}
	@Test(priority = 3)
	public void test_003IdDetailsPage() throws Exception 
	{
		new IdDetails(driver,excel);
	}
	@Test(priority = 4)
	public void test_004RegistrationFeesPage() throws Exception 
	{
		new RegistrationFees(driver,excel);
	}
	@Test(priority = 5)
	public void test_005LegalAutorizationPage()
	{
		new LegalAuthorization(driver);
	}
	@Test(priority = 6)
	public void test_006BasicDetailsPage() throws Exception
	{
		new BasicDetails(driver,excel);
	}
	@Test(priority = 7)
	public void test_007SetPasswordPage() throws Exception
	{
		new SetPassword(driver);
	}
	@Test(priority = 9)
	public void test_009ConfirmAddress()
	{
		new ConfirmAddress(driver,excel);
	}
	@Test(priority = 8)
	public void test_008AddBankAccountPage() throws Exception
	{
		new AddBankAccount(driver,excel);
	}
	@Test(priority = 10)
	public void test_0010LiveKYCPage()
	{
		new LiveKYC(driver);
	}
	
}




