package com.lendenclub.utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.SkipException;

import com.lendenclub.testclass.Investor_Registration;

public class ScreenshotHandler implements ITestListener 
{

	@Override
	    public void onTestStart(ITestResult iTestResult) {}

	    @Override
	    public void onTestSuccess(ITestResult iTestResult) {}

	    @Override
	    public void onTestFailure(ITestResult iTestResult) {
	    	
	    	try {
		        
			//Date format for screenshot file name
		       SimpleDateFormat df=new  SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		       Date date =new Date();
		       String fileName = df.format(date);
		       File file = ((TakesScreenshot)Investor_Registration.getDriver()).getScreenshotAs(OutputType.FILE);
		        //coppy screenshot file into screenshot folder.
		        FileUtils.copyFile(file, new File(System.getProperty("user.dir")+"//Screenshot//"+fileName+".jpg"));
		        System.out.println("Screenshot is capatured");
		        
		     //   Assert.assertTrue(false); 
		       // throw new SkipException("Test skipped");
		      //  Investor_Registration.getDriver().sendSMS("7506154250", "hello world");
		        
			
	    	}catch(Exception e) {
				e.printStackTrace();
			}
	    	
	    	//Copy File from Source folder to Destination folder
//			File source = new File("/home/shubhamsharma/eclipse-workspace/investor_Registration/Screenshot");
//
//			File dest = new File("/home/shubhamsharma/AutomationTesting/Screenshots");
//			
//			
//			try {
//				FileUtils.copyDirectory(source, dest);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			//Multiple Screenshots reduced to 1 in Source folder 
//			int no_of_files = source.listFiles().length;
//			System.out.println("no_of_files:"+no_of_files);
//			
//			for(int i=0; i< no_of_files - 1; i++)
//			{
//				File file = source.listFiles()[i];
//				System.out.println(file.getAbsolutePath());
//				file.delete();
//			}
//				
 	    }
	    
		@Override
	    public void onTestSkipped(ITestResult iTestResult) {}

	    @Override
	    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {}

	    @Override
	    public void onStart(ITestContext iTestContext) {
	    	
	    	File source = new File("./Screenshot");
	    	//File source = new File("/home/shubham/eclipse-workspace/investor_Registration/Screenshot");
			try {
				FileUtils.cleanDirectory(source);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
	    }

	    @Override
	    public void onFinish(ITestContext iTestContext) {}
	
	
}
