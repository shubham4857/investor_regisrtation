package com.lendenclub.registration.steps;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Onboarding 
{

	public Onboarding(AndroidDriver<MobileElement> driver) 
	{
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.MINUTES);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Next']")).click();
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Next']")).click();
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Next']")).click();
	}

}
