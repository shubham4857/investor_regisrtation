package com.lendenclub.registration.steps;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.lendenclub.utility.ExcelSheet;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class IdDetails {
	
	
	public IdDetails(AndroidDriver<MobileElement>driver,ExcelSheet excel)
	{

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//		try {
//			WebDriverWait wait = new WebDriverWait(driver, 2);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36")));
//		} catch (Exception ex) {
//			ex.getMessage();
//		}
		//entering pan card number
		driver.findElement(By.xpath("//android.widget.EditText[@text='PAN Card Number']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 4));
		//ADSCS3746A
		driver.findElement(By.xpath("//android.widget.EditText[@text='DD/MM/YYYY']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 5));
		
		driver.findElement(By.xpath("//android.widget.TextView[@text='Submit Details']")).click();
		
	}
	
}
