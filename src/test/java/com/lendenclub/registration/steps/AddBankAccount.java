package com.lendenclub.registration.steps;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lendenclub.utility.ExcelSheet;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class AddBankAccount {

	public AddBankAccount(AndroidDriver<MobileElement> driver, ExcelSheet excel) throws Exception {

//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ImageView"))
				.click();
		}catch(Exception e)  { }
		try {
			WebDriverWait wait = new WebDriverWait(driver, 6);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402cf4651d5f")));
		} catch (org.openqa.selenium.TimeoutException ex) {
			ex.getMessage();
		}
		
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.EditText[@text='Bank A/C Number']"))
				.sendKeys(excel.ReadCell(excel.GetCell("HP"), 12));
		driver.findElement(By.xpath("//android.widget.EditText[@text='Confirm Bank A/C Number']"))
				.sendKeys(excel.ReadCell(excel.GetCell("HP"), 12));
		driver.findElement(By.xpath("//android.widget.TextView[@text='Select Account Type']")).click();

		driver.findElement(By.xpath("//android.widget.CheckedTextView[@index='1']")).click();
		driver.findElement(By.xpath("//android.widget.EditText[@text='Your Name as per Bank Account']"))
				.sendKeys(excel.ReadCell(excel.GetCell("HP"), 14));
		// for scroll page
		// page.ByPointer(530, 450, 530, 190, driver);

		TouchAction touch = new TouchAction(driver);
		touch.press(PointOption.point(530, 450)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
				.moveTo(PointOption.point(530, 190)).release().perform();
		// IFSC
		driver.findElement(By.xpath("//android.widget.EditText[@text='IFSC']"))
				.sendKeys(excel.ReadCell(excel.GetCell("HP"), 13));
		driver.findElement(By.xpath("//android.widget.TextView[@text='Submit']")).click();
	}
}
