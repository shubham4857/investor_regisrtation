package com.lendenclub.registration.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class SetPassword 
{
	
	public SetPassword(AndroidDriver<MobileElement> driver) throws Exception 
	{	
		
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.findElement((By.xpath("//android.view.ViewGroup[@index='4']"))).click();
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_4));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_8));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_7));	
			
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElement((By.xpath("//android.view.ViewGroup[@index='4']"))).click();
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_4));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_8));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_7));	
			
		
		
		WebDriverWait wait2=new WebDriverWait(driver,30);
		WebElement element2=wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@index='4']")));
		element2.click();
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_4));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_8));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_7));
	
	}

}
