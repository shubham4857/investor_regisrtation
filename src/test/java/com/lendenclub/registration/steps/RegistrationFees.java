package com.lendenclub.registration.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import com.lendenclub.utility.ExcelSheet;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class RegistrationFees 
{
	
	MobileElement element;
	
	public RegistrationFees(AndroidDriver<MobileElement>driver,ExcelSheet excel) throws Exception 
	{
		//select type
	//	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget"
				+ ".FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view"
				+ ".ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.HorizontalScrollView/android"
				+ ".view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[10]/android.widget.TextView\n"+ "")).click();
	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		   driver.findElement(By.xpath("//android.widget.TextView[@text='Proceed to Pay 500']")).click();
	       driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES); 
	       driver.findElement(By.xpath("//android.widget.EditText[@index='1']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 1));
	    
	       element = (MobileElement) driver.findElement(
					MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).setAsVerticalList()"
					+ ".scrollIntoView(new UiSelector().text(\"Next\"))"));  
	       driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	       driver.findElement(By.xpath("//android.widget.Button[@text='Next']")).click();
		   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   driver.findElement(By.xpath("//android.widget.TextView[@text='Credit Card']")).click(); 
		   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).click();
		   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 6));
		   driver.pressKey(new KeyEvent(AndroidKey.TAB));
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_0));
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		   Thread.sleep(2000);
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_2));
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		   Thread.sleep(2000);
		   //cvv
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_1));
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_1));
		   driver.pressKey(new KeyEvent(AndroidKey.DIGIT_1));
		   
			//click on pay 
		   try {
		  
			   driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			   driver.findElement(By.xpath("//android.widget.Button[@index='3']")).click();
		       
		   } catch(Exception e)
		   {
			 
			   driver.findElement(By.xpath("//android.widget.Button[@text='PAY ₹500']")).click();
			  // driver.findElement(By.id("submit-form-button")).click();
		   }
		   	
		   Thread.sleep(4000);
		   //Enter card password
		   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 7));
		  
		  //click on submit button
		   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		   driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click();
		
	}

}
