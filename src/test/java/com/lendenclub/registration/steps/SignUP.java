package com.lendenclub.registration.steps;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.lendenclub.utility.ExcelSheet;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class SignUP
{
	
	public SignUP(AndroidDriver<MobileElement> driver,ExcelSheet excel) throws Exception
	{

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Sign Up']")).click();
		//ENTER BASIC DETAILS
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.EditText[@text='Full Name as per your PAN']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 1));
	    driver.findElement(By.xpath("//android.widget.EditText[@text='Enter Mobile Number']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 2));
		driver.findElement(By.xpath("//android.widget.EditText[@text='Email ID']")).sendKeys(excel.ReadCell(excel.GetCell("HP"), 3));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Send OTP']")).click();
		
		Thread.sleep(2000);
		//validate mobile number with OTP verification
		try
		{
		driver.hideKeyboard();	
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
		driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).click();
		Thread.sleep(2000);
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_6));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_6));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_1));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_1));
		
		Thread.sleep(2000); 			
		driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).clear();
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_6));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_6));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_3));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_9));
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).clear();
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_6));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_6));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_5));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_9));
		driver.pressKey(new KeyEvent(AndroidKey.DIGIT_0));
		
		
		}catch(Exception e){
			System.out.println(e.getCause());
		}
	}
	
	
}
