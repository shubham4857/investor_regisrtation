package com.lendenclub.registration.steps;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.lendenclub.utility.ExcelSheet;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class BasicDetails {

	MobileElement element;

	public BasicDetails(AndroidDriver<MobileElement> driver, ExcelSheet excel) throws Exception {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Complete Basic Details']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget"
		// +
		// ".FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget"
		// +
		// ".ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view"
		// + ".ViewGroup[1]/android.view.ViewGroup\n")).click();

		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup"))
		.click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Single']")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Aadhaar Card']")).click();
		// for scroll
		TouchAction touch = new TouchAction(driver);
		touch.press(PointOption.point(560, 830)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
		.moveTo(PointOption.point(560, 250)).release().perform();

		// page.ByPointer(560, 830, 560, 250, driver);
		driver.findElement(By.xpath("//android.widget.ImageView[@index='10']")).click();
		driver.findElement(By.xpath("//android.widget.Button[@text='GALLERY']")).click();

		// for allow button
		try {
			driver.findElement(By.xpath("//android.widget.Button[@index='0']")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//android.widget.Button[@index='0']")).click();
		} catch (Exception e) {
		}

		try {
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button")).click();
		} catch (Exception e) {
		}

		try {
			driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView"))
			.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// For scroll

			element = (MobileElement) driver.findElement(MobileBy
					.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).setAsVerticalList()"
							+ ".scrollIntoView(new UiSelector().text(\"Aadhaar Card\"))"));

			driver.findElement(By.xpath("//android.widget.TextView[@text='Aadhaar Card']")).click();
			driver.findElement(By.xpath("//com.oplus.gallery.business_lib.ui.view.SlotView[@index='1']")).click();
		} catch (Exception e) {
		}

		// upload back Aadhaar card

		WebDriverWait wait = new WebDriverWait(driver, 40);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//android.widget.ImageView[@index='12']")));
		element.click();

		try {
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//android.widget.Button[@text='GALLERY']")).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.TabHost/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.oplus.widget.OplusViewPager/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView"))
			.click();

			element= (MobileElement) driver.findElement(
					MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).setAsVerticalList()"
							+ ".scrollIntoView(new UiSelector().text(\"Aadhaar Card\"))"));

			driver.findElement(By.xpath("//android.widget.TextView[@text='Aadhaar Card']")).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//com.oplus.gallery.business_lib.ui.view.SlotView[@index='0']")).click();
		} catch (Exception e) {
			System.out.println("please upload Adhaar card manualy");
		}

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Permanent']")).click();
		driver.findElement(By.xpath("//android.widget.EditText[@text='ID Number']"))
		.sendKeys(excel.ReadCell(excel.GetCell("HP"), 8));
		driver.hideKeyboard();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
		driver.findElement(By.xpath("//android.widget.TextView[@text='Submit']")).click();
		} catch(Exception e) { }
		try {
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")).click();		
		} catch(Exception e) { }

	}

}
