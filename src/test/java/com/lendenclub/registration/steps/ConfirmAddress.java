package com.lendenclub.registration.steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.lendenclub.utility.ExcelSheet;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ConfirmAddress {

	public ConfirmAddress(AndroidDriver<MobileElement> driver, ExcelSheet excel) {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android"
		//				+ ".view.ViewGroup/android.widget.ImageView")).click();
		//		driver.findElement(By.xpath("//android.widget.TextView[@text='Basic Details']")).click();
		try {
			driver.findElement(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView"))
			.click();
		} catch(Exception e) { }
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402cf4651d5f")));
		} catch (org.openqa.selenium.TimeoutException ex) { }
			
		//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.EditText[@text='Complete Address']"))
		.sendKeys(excel.ReadCell(excel.GetCell("HP"), 9));
		driver.findElement(By.xpath("//android.widget.EditText[@text='Landmark']"))
		.sendKeys(excel.ReadCell(excel.GetCell("HP"), 10));
		driver.findElement(By.xpath("//android.widget.EditText[@text='PIN Code']"))
		.sendKeys(excel.ReadCell(excel.GetCell("HP"), 11));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Submit']")).click();
	}

}
